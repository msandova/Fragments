<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2020 Felix Häcker -->
<component type="desktop-application">
  <id>de.haeckerfelix.Fragments</id>
  <name>Fragments</name>
  <summary>A BitTorrent Client</summary>
  <translation type="gettext">fragments</translation>
  <developer_name>Felix Häcker</developer_name>
  <update_contact>haecker.felix1207@gmail.com</update_contact>
  <metadata_license>CC0</metadata_license>
  <project_license>GPL-3.0+</project_license>

  <description>
    <p>
      Fragments is an easy to use BitTorrent client for the GNOME desktop
      environment. It is usable for receiving files using the BitTorrent
      protocol, which enables you to transmit huge files, like videos or
      installation images for Linux distributions.
    </p>
  </description>

  <screenshots>
    <screenshot type="default">
      <image width="745" height="480">https://raw.githubusercontent.com/haecker-felix/Fragments/master/data/screenshots/1.png</image>
    </screenshot>
    <screenshot>
      <image width="745" height="480">https://raw.githubusercontent.com/haecker-felix/Fragments/master/data/screenshots/2.png</image>
    </screenshot>
    <screenshot>
      <image width="745" height="480">https://raw.githubusercontent.com/haecker-felix/Fragments/master/data/screenshots/3.png</image>
    </screenshot>
  </screenshots>
  <launchable type="desktop-id">de.haeckerfelix.Fragments.desktop</launchable>
  <url type="homepage">https://gitlab.gnome.org/World/Fragments</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/Fragments/issues</url>
  <url type="donation">https://de.liberapay.com/haecker-felix</url>
  <url type="translate">https://l10n.gnome.org/module/Fragments/</url>
  <content_rating type="oars-1.0" />
  <kudos>
    <kudo>HiDpiIcon</kudo>
    <kudo>ModernToolkit</kudo>
    <kudo>Notifications</kudo>
  </kudos>

  <custom>
    <value key="Purism::form_factor">workstation</value>
    <value key="Purism::form_factor">mobile</value>
  </custom>

  <releases>
    <release version="1.5" date="2020-09-13">
      <description>
        <p>
          New features / changes:
        </p>
        <ul>
          <li>Show notifications when a new torrent is added, or when a download is complete</li>
          <li>The encryption mode can now be configured (Forced, Optional, Disabled)</li>
          <li>New menu entry to remove all downloaded torrents</li>
          <li>Refreshed user interface / Updated application icon</li>
          <li>Updated translations</li>
        </ul>
      </description>
    </release>
    <release version="1.4" date="2019-03-24">
      <description>
        <p>
          New features / changes:
        </p>
        <ul>
          <li>New application icon + theme</li>
          <li>New 'Open' button to open downloaded torrents faster</li>
          <li>Updated translations</li>
          <li>Several bug fixes</li>
        </ul>
      </description>
    </release>
    <release version="1.3" date="2018-12-31">
      <description>
        <p>
          New features / changes:
        </p>
        <ul>
          <li>New adaptive settings dialog</li>
          <li>Small interface improvements</li>
          <li>Update libtransmission backend to 3.00</li>
          <li>Updated translations</li>
        </ul>
      </description>
    </release>
    <release version="1.2" date="2018-09-16">
      <description>
        <p>
          New features / changes:
        </p>
        <ul>
          <li>Adaptive interface (Ready for Librem5!)</li>
          <li>Move AppMenu to headerbar</li>
          <li>Added translations: cs, de, fr, nb_NO, nl, tr</li>
        </ul>
      </description>
    </release>
    <release version="1.1" date="2018-04-22">
      <description>
        <p>
          This version contains minor technical changes.
        </p>
        <ul>
          <li>Fix blurry welcome image</li>
          <li>Updated transmission submodule</li>
        </ul>
      </description>
    </release>
    <release version="1.0" date="2018-04-06">
      <description>
        <p>
          This is the first stable release of Fragments.
        </p>
        <ul>
          <li>Support for torrent files</li>
          <li>Support for magnet links</li>
          <li>Automatically detects magnet links in your clipboard</li>
          <li>Simple queue management. Torrents can be reordered using drag and drop.</li>
          <li>Clean and structured user interface</li>
        </ul>
      </description>
    </release>
  </releases>

</component>


