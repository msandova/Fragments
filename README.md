# Fragments

![screenshot](https://gitlab.gnome.org/World/Fragments/raw/master/data/screenshots/1.png)
    
Fragments is an easy to use BitTorrent client for the GNOME desktop environment. It is usable for receiving files using the BitTorrent protocol, which enables you to transmit huge files, like videos or installation images for Linux distributions.

## Getting in Touch
If you have any questions regarding the use or development of Fragments,
want to discuss design or simply hang out, please join us on our [#fragments:matrix.org](https://matrix.to/#/#fragments:matrix.org) channel.

## Translations
Translation of this project takes place on the GNOME translation platform,
[Damned Lies](https://l10n.gnome.org/module/Fragments). For further
information on how to join a language team, or even to create one, please see
[GNOME Translation Project wiki page](https://wiki.gnome.org/TranslationProject).

## Install
Make sure you have Flatpak installed. [Get more information](http://flatpak.org/getting.html)

<a href='https://flathub.org/apps/details/de.haeckerfelix.Fragments'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>

## Building
Fragments can be built and run with [Gnome Builder](https://wiki.gnome.org/Apps/Builder) >= 3.28.
Just clone the repo and hit the run button!
You can get Builder from [here](https://wiki.gnome.org/Apps/Builder/Downloads).

You also can build Fragments manually without using Flatpak:

```
git clone https://gitlab.gnome.org/World/Fragments
cd Fragments
mkdir build
cd build
meson ..
ninja
sudo ninja install
```

**Note:** Additional to the standard dependencies (GTK, Vala, ...), you need following dependencies:
- libtransmission
- libnatpmp
- libevent
- libb64
- dht
- miniupnpc
- libutp
- libhandy

For additionals details of those dependencies you can check the [Flatpak manifest](https://gitlab.gnome.org/World/Fragments/blob/master/de.haeckerfelix.Fragments.json)

## Code Of Conduct
We follow the [GNOME Code of Conduct](/CODE_OF_CONDUCT.mdmd).
All communications in project spaces are expected to follow it.

