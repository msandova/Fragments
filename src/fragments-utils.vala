[DBus (name = "org.freedesktop.FileManager1")]
public interface org.freedesktop.FileManager1 : Object {
	public const string NAME = "org.freedesktop.FileManager1";
	public const string PATH = "/org/freedesktop/FileManager1";

	public abstract async void show_folders(string[] uris, string startup_id) throws IOError, DBusError;
	public abstract async void show_items(string[] uris, string startup_id) throws IOError, DBusError;
	public abstract async void show_item_properties(string[] uris, string startup_id) throws IOError, DBusError;
}

public class Fragments.Utils{

	public static string time_to_string (uint total_seconds) {
		if (total_seconds > 86400) return _("more than a day");

		uint seconds = (total_seconds % 60);
		uint minutes = (total_seconds % 3600) / 60;
		uint hours = (total_seconds % 86400) / 3600;
		uint days = (total_seconds % (86400 * 30)) / 86400;

		var str_days = ngettext ("%u day", "%u days", days).printf (days);
		var str_hours = ngettext ("%u hour", "%u hours", hours).printf (hours);
		var str_minutes = ngettext ("%u minute", "%u minutes", minutes).printf (minutes);
		var str_seconds = ngettext ("%u second", "%u seconds", seconds).printf (seconds);

		if (days > 0) return "%s, %s".printf (str_days, str_hours);
		if (hours > 0) return "%s, %s".printf (str_hours, str_minutes);
		if (minutes > 0) return str_minutes;
		if (seconds > 0) return str_seconds;
		return "";
	}

	public static string generate_primary_text(Torrent torrent){
		if (torrent.downloaded == torrent.size)
			/* Translators: First %s is the amount uploaded, second %s is the
			 * uploading speed
			 * Displayed under the torrent name in the main window when torrent
			 * download is finished */
			return _("%s uploaded · %s").printf(torrent.uploaded, torrent.upload_speed);
		else if (torrent.activity == Transmission.Activity.STOPPED || torrent.activity == Transmission.Activity.DOWNLOAD_WAIT)
			/* Translators: First %s is the amount downloaded, second %s is the
			 * total torrent size
			 * Displayed under the torrent name in the main window when torrent
			 * is stopped */
			return _("%s of %s").printf(torrent.downloaded, torrent.size);
		else
			/* Translators: First %s is the amount downloaded, second %s is the
			 * total torrent size, third %s is the download speed
			 * Displayed under the torrent name in the main window when torrent
			 * is downloading */
			return _("%s of %s · %s").printf(torrent.downloaded, torrent.size, torrent.download_speed);
	}

	public static string generate_secondary_text(Torrent torrent){
		string st = "";
		switch(torrent.activity){
			case Transmission.Activity.STOPPED: { st = _("Paused"); break;}
			case Transmission.Activity.DOWNLOAD: {
				if(torrent.eta != uint.MAX && torrent.eta != 0 && torrent.eta != 4294967294)
					st = "%s".printf(Utils.time_to_string(torrent.eta));
				break;}
			case Transmission.Activity.DOWNLOAD_WAIT: { st = C_("Status of a single torrent", "Queued"); break;}
			case Transmission.Activity.CHECK: { st = _("Checking…"); break;}
			case Transmission.Activity.CHECK_WAIT: { st = C_("Status of a single torrent", "Queued"); break;}
		}
		return st;
	}

	public static string get_clipboard_text(Window window){
		Gdk.Display display = window.get_display ();
		Gtk.Clipboard clipboard = Gtk.Clipboard.get_for_display (display, Gdk.SELECTION_CLIPBOARD);

		return clipboard.wait_for_text ();
	}

	public static void clear_clipboard(Window window){
		Gdk.Display display = window.get_display ();
		Gtk.Clipboard clipboard = Gtk.Clipboard.get_for_display (display, Gdk.SELECTION_CLIPBOARD);
		clipboard.set_text("", 0);
	}

	public static async void show_file_in_filemanager(File file) throws Error {
		try {
			org.freedesktop.FileManager1? manager = yield Bus.get_proxy (BusType.SESSION,
																		 org.freedesktop.FileManager1.NAME,
																		 org.freedesktop.FileManager1.PATH,
																		 DBusProxyFlags.DO_NOT_LOAD_PROPERTIES |
																		 DBusProxyFlags.DO_NOT_CONNECT_SIGNALS);
			var id = "%s_%s_%d_%s".printf(Environment.get_prgname(), Environment.get_host_name(),
										  Posix.getpid(), new DateTime.now_local().format_iso8601());
			yield manager.show_items({file.get_uri()}, id);
		} catch (Error e) {
			warning("Failed to launch file manager using DBus, using fall-back: %s", e.message);
			Gtk.show_uri_on_window(App.window, file.get_parent().get_uri(), Gdk.CURRENT_TIME);
		}
	}

	public static void send_notification(string title, string subtitle, string icon = ""){
		message("Send new notification: %s (%s)", title, subtitle);
		var notification = new Notification (title);
		notification.set_body(subtitle);

		if (icon != ""){
			try{
				notification.set_icon(Icon.new_for_string(icon));
			}catch (Error e){
				warning("Unable to display notification: %s", e.message);
			}
		}

		App.window.get_application().send_notification(title+subtitle, notification);
	}
}
