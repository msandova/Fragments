using Gtk;
using Hdy;

[GtkTemplate (ui = "/de/haeckerfelix/Fragments/ui/settings-window.ui")]
public class Fragments.SettingsWindow : Hdy.PreferencesWindow {

	[GtkChild] private Switch dark_theme_switch;
	[GtkChild] private SpinButton max_downloads_spinbutton;
	[GtkChild] private Button download_folder_button;
	[GtkChild] private Label download_folder_label;
	[GtkChild] private Switch notifications_downloaded_switch;
	[GtkChild] private Switch notifications_new_torrent_switch;
	[GtkChild] private RadioButton encryption_clear_rbutton;
	[GtkChild] private RadioButton encryption_prefer_rbutton;
	[GtkChild] private RadioButton encryption_force_rbutton;

	public SettingsWindow () {
		connect_signals();

		switch (App.settings.encryption_mode) {
			case Transmission.EncryptionMode.CLEAR_PREFERRED: encryption_clear_rbutton.set_active(true); break;
			case Transmission.EncryptionMode.ENCRYPTION_PREFERRED: encryption_prefer_rbutton.set_active(true); break;
			case Transmission.EncryptionMode.ENCRYPTION_REQUIRED: encryption_force_rbutton.set_active(true); break;
		}
	}

	private void connect_signals(){
		App.settings.bind("enable-dark-theme", dark_theme_switch, "active", SettingsBindFlags.DEFAULT);
		App.settings.bind("max-downloads", max_downloads_spinbutton, "value", SettingsBindFlags.DEFAULT);
		App.settings.bind("download-folder", download_folder_label, "label", SettingsBindFlags.DEFAULT);
		App.settings.bind("enable-notifications-downloaded", notifications_downloaded_switch, "active", SettingsBindFlags.DEFAULT);
		App.settings.bind("enable-notifications-new-torrent", notifications_new_torrent_switch, "active", SettingsBindFlags.DEFAULT);

		download_folder_button.clicked.connect(() => {
			// Translators: Title of the file chooser to select a torrent
			Gtk.FileChooserDialog chooser = new Gtk.FileChooserDialog (_("Select download folder"), this, Gtk.FileChooserAction.SELECT_FOLDER);
			// Translators: Button of the file chooser to select a torrent
			chooser.add_button (_("Cancel"), Gtk.ResponseType.CANCEL);
			// Translators: Button of the file chooser to select a torrent
			chooser.add_button (_("Open"), Gtk.ResponseType.ACCEPT);
			chooser.set_default_response (Gtk.ResponseType.ACCEPT);
				chooser.set_current_folder(App.settings.download_folder);

			if (chooser.run () == Gtk.ResponseType.ACCEPT) {
				App.settings.download_folder = chooser.get_current_folder();
				download_folder_label.set_text(chooser.get_current_folder());
			}

			chooser.close ();
			chooser.destroy();
		});

		encryption_clear_rbutton.toggled.connect(() => {
			if (encryption_clear_rbutton.active) App.settings.encryption_mode = Transmission.EncryptionMode.CLEAR_PREFERRED;
		});
		encryption_prefer_rbutton.toggled.connect(() => {
			if (encryption_prefer_rbutton.active) App.settings.encryption_mode = Transmission.EncryptionMode.ENCRYPTION_PREFERRED;
		});
		encryption_force_rbutton.toggled.connect(() => {
			if (encryption_force_rbutton.active) App.settings.encryption_mode = Transmission.EncryptionMode.ENCRYPTION_REQUIRED;
		});
	}
}

