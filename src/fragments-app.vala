using Gtk;
using GLib;

public class Fragments.App : Gtk.Application {

	public static Fragments.Window window;
	public static Settings settings;

	private TorrentManager manager;

	public App(){
		application_id = "de.haeckerfelix.Fragments"; flags = ApplicationFlags.HANDLES_OPEN;
	}

	protected override void startup () {
		base.startup ();

		settings = new Settings();
		setup_actions();
		manager = new TorrentManager();
	}

	protected override void activate (){
		ensure_window();
		window.present();
	}

	public override void open (File[] files, string hint) {
		activate();

		if (files[0].has_uri_scheme ("magnet")) {
			var magnet = files[0].get_uri ();
			magnet = magnet.replace ("magnet:///?", "magnet:?");
			manager.add_torrent_by_magnet(magnet);
		}else{
			foreach (var file in files) {
				manager.add_torrent_by_path (file.get_path());
			}
		}
	}

	private void ensure_window(){
		if (get_windows () != null) return;

		window = new Fragments.Window(this, ref manager);
		this.add_window(window);
	}

	private void setup_actions () {
		var action = new GLib.SimpleAction ("preferences", null);
		action.activate.connect (() => {
			var settings_window = new SettingsWindow();
			settings_window.set_transient_for(App.window);
			settings_window.set_modal(true);
			settings_window.set_visible(true);
		});
		this.add_action (action);

		action = new GLib.SimpleAction ("about", null);
		action.activate.connect (() => { this.show_about_dialog (); });
		this.add_action (action);

		action = new GLib.SimpleAction ("quit", null);
		action.activate.connect (this.quit);
		this.add_action (action);

		action = new GLib.SimpleAction ("remove-completed-torrents", null);
		action.activate.connect (() => { this.remove_completed_torrents (); });
		this.add_action (action);
	}

	private void remove_completed_torrents() {
		Gtk.MessageDialog msg = new Gtk.MessageDialog (App.window, Gtk.DialogFlags.MODAL, Gtk.MessageType.QUESTION, Gtk.ButtonsType.NONE, "");

		// Translators: Text displayed in the modal when the user removes all finished torrents
		msg.secondary_text = _("This will only remove torrents from Fragments, but will keep the downloaded content.");
		// Translators: Title of the modal when the user removes all finished torrents
		msg.text = _("Remove all finished Torrents?");

		// Translators: Button in the modal when the user removes all finished torrents
		msg.add_button(_("Cancel"), 0);
		// Translators: Button in the modal when the user removes all finished torrents
		msg.add_button(_("Remove"), 1);

		msg.response.connect ((response_id) => {
			if(response_id == 1){
				manager.seed_torrents.clear();
			}
			msg.destroy();
		});
		msg.show ();
	}

	private void show_about_dialog(){
		string[] authors = {
			"Felix Häcker <haeckerfelix@gnome.org>",
			"Bilal Elmoussaoui <bilal.elmoussaoui@gnome.org>"
		};

		string[] artists = {
			"Tobias Bernard",
			"Jakub Steiner",
			"Sam Hewitt",
		};

		Gtk.show_about_dialog (window,
			logo_icon_name: "de.haeckerfelix.Fragments",
			program_name: "Fragments",
			comments: ("A BitTorrent Client"),
			copyright: "© 2018 - 2020 Felix Häcker",
			authors: authors,
			artists: artists,
			website: "https://gitlab.gnome.org/World/Fragments",
			website_label: ("GitLab Homepage"),
			version: Config.VERSION,
			license_type: License.GPL_3_0);
	}

	public static int main (string[] args){
		// Setup gettext
		Intl.bindtextdomain(Config.GETTEXT_PACKAGE, Config.GNOMELOCALEDIR);
		Intl.setlocale(LocaleCategory.ALL, "");
		Intl.textdomain(Config.GETTEXT_PACKAGE);
		Intl.bind_textdomain_codeset(Config.GETTEXT_PACKAGE, "utf-8");

		Gtk.init(ref args);
		Hdy.init();

		var app = new App ();
		return app.run(args);
	}
}
