public class Fragments.Settings : GLib.Settings{

	public Settings(){
		Object(schema_id: "de.haeckerfelix.Fragments");
	}

	public bool enable_dark_theme{
		get{ return get_boolean("enable-dark-theme"); }
		set{ set_boolean ("enable-dark-theme", value); }
	}

	public string download_folder{
		owned get{ return get_string("download-folder"); }
		set{ set_string ("download-folder", value); }
	}

	public string incomplete_folder{
		owned get{ return get_string("incomplete-folder"); }
		set{ set_string ("incomplete-folder", value); }
	}

	public int max_downloads{
		get{ return get_int("max-downloads"); }
		set{ set_int ("max-downloads", value); }
	}

	public bool enable_notifications_downloaded{
		get{ return get_boolean("enable-notifications-downloaded");	}
		set{ set_boolean ("enable-notifications-downloaded", value); }
	}

	public bool enable_notifications_new_torrent{
		get{ return get_boolean("enable-notifications-new-torrent"); }
		set{ set_boolean ("enable-notifications-new-torrent", value); }
	}

	public Transmission.EncryptionMode encryption_mode{
		get{ return (Transmission.EncryptionMode)get_int("encryption-mode"); }
		set{ set_int ("encryption-mode", value); }
	}
}

