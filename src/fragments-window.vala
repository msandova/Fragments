using Gtk;
using Hdy;

[GtkTemplate (ui = "/de/haeckerfelix/Fragments/ui/window.ui")]
public class Fragments.Window : Hdy.ApplicationWindow {

	private TorrentManager manager;

	[GtkChild] private Stack notification_stack;
	[GtkChild] private Stack window_stack;
	[GtkChild] private Label magnet_notification_label;
	[GtkChild] private Revealer notification_revealer;
	[GtkChild] private Viewport torrent_viewport;
	private Clamp torrent_group_box;

	public Window (App app, ref TorrentManager manager) {
		GLib.Object(application: app);

		this.manager = manager;
		this.show_all();

		var provider = new CssProvider ();
		provider.load_from_resource ("/de/haeckerfelix/Fragments/interface/adwaita.css");
		StyleContext.add_provider_for_screen (Gdk.Screen.get_default (), provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);

		TorrentGroup downloading_group = new TorrentGroup(C_("Title of a list of torrents in the main window", "Downloading"));
		TorrentGroup seeding_group = new TorrentGroup(C_("Title of a list of torrents in the main window","Seeding"));
		TorrentGroup queued_group = new TorrentGroup(C_("Title of a list of torrents in the main window","Queued"));

		Box groups = new Box(Orientation.VERTICAL, 0);
		groups.spacing = 22;
		groups.margin_bottom = 22;
		groups.margin_top = 22;
		groups.add(downloading_group);
		groups.add(queued_group);
		groups.add(seeding_group);

		torrent_group_box = new Clamp();
		torrent_group_box.add(groups);
		torrent_group_box.show_all();
		torrent_group_box.set_maximum_size(600);

		queued_group.add_subgroup(manager.download_wait_torrents, true);
		queued_group.add_subgroup(manager.stopped_torrents, false);
		queued_group.add_subgroup(manager.check_wait_torrents, false);
		queued_group.add_subgroup(manager.check_torrents, false);

		downloading_group.add_subgroup(manager.download_torrents, false);
		seeding_group.add_subgroup(manager.seed_torrents, false);

		torrent_viewport.add(torrent_group_box);

		update_stack_page();
		connect_signals();
	}

	private void connect_signals(){
		App.settings.bind("enable-dark-theme", Gtk.Settings.get_default(),"gtk-application-prefer-dark-theme", SettingsBindFlags.GET);
		manager.notify["torrent-count"].connect(update_stack_page);
		this.focus_in_event.connect(check_for_magnet_link);
	}

	private void update_stack_page(){
		if(manager.torrent_count == 0)
			window_stack.set_visible_child_name("empty");
		else
			window_stack.set_visible_child_name("content");
	}

	protected override bool delete_event(Gdk.EventAny eventany){
		Timeout.add_seconds(2, () => { // Wait a bit, until the window get's hidden.
			manager.close_session();
			this.get_application().quit();
			return false;
		});
		this.hide();
		return true;
	}

	[GtkCallback]
	private void open_torrent_button_clicked(){
		// Translators: Title of the file chooser when adding a torrent
		var filech = new Gtk.FileChooserNative (_("Open torrents"), this, Gtk.FileChooserAction.OPEN, null, null);
		filech.set_select_multiple (true);
		filech.set_current_folder_uri (GLib.Environment.get_home_dir ());

		var all_files_filter = new Gtk.FileFilter ();
		// Translators: Name of the filter in the file chooser when adding a torrent
		all_files_filter.set_filter_name (_("All files"));
		all_files_filter.add_pattern ("*");
		var torrent_files_filter = new Gtk.FileFilter ();
		// Translators: Name of the filter in the file chooser when adding a torrent
		torrent_files_filter.set_filter_name (_("Torrent files"));
		torrent_files_filter.add_mime_type ("application/x-bittorrent");
		filech.add_filter (torrent_files_filter);
		filech.add_filter (all_files_filter);

		if (filech.run () == Gtk.ResponseType.ACCEPT) {
			foreach (string filename in filech.get_filenames()) {
				manager.add_torrent_by_path(filename);
			}
		}
	}

	private bool check_for_magnet_link(){
		message("Check for magnet link in clipboard...");

		string clipboard_text = Utils.get_clipboard_text(this);
		if(clipboard_text == null) return false;
		string torrent_name = manager.get_magnet_name(clipboard_text);

		if(torrent_name != null && torrent_name != ""){
			message("Detected torrent: " + torrent_name);
			message(manager.contains_magnet_link(clipboard_text).to_string());
			if(manager.contains_magnet_link(clipboard_text) == false){
				show_magnet_notification(torrent_name);
			} else {
				message("Detected torrent was added before.");
			}
		}

		return false;
	}

	private void show_magnet_notification(string torrent_name){
		notification_stack.set_visible_child_name("add-magnet");
		magnet_notification_label.set_markup(_("Add magnet link “%s” from clipboard?").printf(torrent_name));
		notification_revealer.set_reveal_child(true);
	}

	[GtkCallback]
	private void add_magnet_button_clicked(){
		manager.add_torrent_by_magnet(Utils.get_clipboard_text(this));
		Utils.clear_clipboard(this);
		notification_revealer.set_reveal_child(false);
	}

	[GtkCallback]
	private void close_magnet_notification_button_clicked(){
		Utils.clear_clipboard(this);
		notification_revealer.set_reveal_child(false);
	}
}
